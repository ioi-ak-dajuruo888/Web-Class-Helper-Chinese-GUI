<p align="center">
<img src="https://ren-yc.github.io/assets/ClassTools/WCH_big.png" width="200">
</p>

<h1 align="center">- 网课助手 中文图形界面版 -</h1>

<p align="center">
<img src="https://img.shields.io/github/v/release/class-tools/Web-Class-Helper.svg?logo=iCloud">
<img src="https://img.shields.io/badge/support-Windows%207%20+-blue?logo=Windows">
</p>

一个在网课期间实用的小工具，原版为英文命令行版，该版本将其汉化并加入图形界面。

[English Command Version](https://github.com/class-tools/Web-Class-Helper)

### 贡献名单

| <img src="https://avatars.githubusercontent.com/u/53416099?v=4" width="60px"></br>ren-yc (Yuchen Ren) | <img src="https://avatars.githubusercontent.com/u/68993466?v=4" width="60px"></br>jsh-jsh (jinshuhang) | <img src="https://avatars.githubusercontent.com/u/95965215?s=40&v=4" width="60px"></br> ioi-ak-dajuruo888 (IOI AK Me！) |
| :-: | :-: | :-: |
| ![img](https://shields.io/badge/Coding-green?logo=visual-studio-code&style=for-the-badge)<br />![img](https://shields.io/badge/BugTester-yellow?logo=open-bug-bounty&style=for-the-badge) | ![img](https://shields.io/badge/Coding-green?logo=visual-studio-code&style=for-the-badge)<br />![img](https://shields.io/badge/BugTester-yellow?logo=open-bug-bounty&style=for-the-badge) | ![img](https://shields.io/badge/Chinese&GUI-green?logo=visual-studio-code&style=for-the-badge)<br />![img](https://shields.io/badge/BugTester-yellow?logo=open-bug-bounty&style=for-the-badge) |

**更多信息请至 [Wiki](https://github.com/ioi-ak-dajuruo888/Web-Class-Helper-Chinese-GUI/wiki)**。

**提交 PR 时请遵循 [CommitMsg 格式要求](https://github.com/ioi-ak-dajuruo888/Web-Class-Helper-Chinese-GUI/blob/master/CommieMsg.md)。**
